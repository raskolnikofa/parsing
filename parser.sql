-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 05 2017 г., 23:47
-- Версия сервера: 5.7.19-0ubuntu0.16.04.1
-- Версия PHP: 7.0.23-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `parser`
--
CREATE DATABASE IF NOT EXISTS `parser` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `parser`;

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `text` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `text`, `img`, `date`) VALUES
(14, 'Students relieve stress through papermaking', 'A creative way to relieve stress made its way to the University this week.    Unit One at Allen Hall is hosting the Peace Paper Project, which is a workshop that incorporates an ancient tradition, papermaking, into self-healing. It is also a way to engage in trauma therapy, social engagement and community activism thro...', 'https://dailyillini.com/wp-content/uploads/2017/10/IMG_9754-475x317.jpg', ' October 5, 2017 '),
(15, 'Turning Point founder Charlie Kirk to speak on socialism', 'Members of Turning Point USA at the University are working to promote their upcoming event, "Charlie Kirk at UIUC Melting Snowflakes and Smashing Socialism Campus Tour," amidst discord and backlash from students.     The event is scheduled to take place tonight from 7 to 8:30 p.m. in the Illini Union.    A...', 'https://dailyillini.com/wp-content/uploads/2017/10/Charlie_Kirk_by_Gage_Skidmore-338x475.jpg', ' October 5, 2017 '),
(16, 'University students travel to China for free', 'A group of 20 University students will travel to Hangzhou, China this summer to take classes and work on green technology as part of the Wanxiang Fellows Program, an agreement between Wanxiang, an automotive parts manufacturing company, and the University system.      Wanxiang began creating programs under the 1...', 'https://dailyillini.com/wp-content/uploads/2017/04/preview-card-news-475x238.jpg', ' October 5, 2017 '),
(17, 'Illinois leads pumpkin production nationwide', 'Illinois currently holds the No. 1 spot for pumpkin production in the nation, producing three times more pumpkins than any other state.    Mohammad Babadoost, professor for ACES, said that Illinois grows and yields more pumpkins than any other state. The state has around 25,000 acres of pumpkins while o...', 'https://dailyillini.com/wp-content/uploads/2017/10/A3_pumpkins-317x475.jpg', ' October 5, 2017 '),
(18, 'Mexico earthquake affects professors at the University', 'On Sept. 19, the people of Mexico suffered as a 7.1 earthquake collapsed buildings and forced many people to abandon their homes. That devastation was not lost on the University as family members of professors lost everything.     Elvira de Mejia, professor of Food Science, said her brother is currently bei...', 'https://dailyillini.com/wp-content/uploads/2017/10/A2_Mexico-475x317.jpg', ' October 5, 2017 '),
(19, 'University continues to achieve recycling initiative', 'Though official percentages for the amount of waste recycled by the University in the 2017 fiscal year have not yet been released, indications show signs of improvement in line with the school’s initiative to recycle 90 percent of its waste by 2020.     Tracy Osby, coordinator of Campus Waste Mana...', 'https://dailyillini.com/wp-content/uploads/2017/10/A1_Recycling_BB-475x317.jpg', ' October 5, 2017 '),
(20, 'Weekly Illini: October 4, 2017', 'The Weekly Illini (formerly known as “The Daily Illini Newscast”) is your news podcast to keep up with some of the Daily Illini’s biggest stories. This episode looks at how our University would handle an active shooter on campus as well as an update to the Brendt Christensen kidnapping case. We...', 'https://dailyillini.com/wp-content/uploads/2017/09/Podcasts-02-1000x1000-1-475x475.png', ' October 4, 2017 '),
(21, 'UI student witnessed aftermath of Vegas attacks', 'Hiam Hafizuddin and two of her friends were running late from the casino to a show they had tickets to early Monday morning. The group, in Las Vegas to celebrate a birthday, got a text from a promoter.     The promoter said not to come because shots were reported near the venue and roads were being c...', 'https://dailyillini.com/wp-content/uploads/2017/10/US_NEWS_LASVEGAS-SHOOTING_17_ABA-475x317.jpg', ' October 4, 2017 '),
(22, 'Gas leak caused by construction', 'An Illini-Alert was sent out this morning around 9:40 a.m. related to a gas leak near the intersection of Fourth and Daniel Streets.    Deputy fire marshal Randy Smith said the leak was reported at 9:15 a.m.    A following Illini-Alert was sent out at 1:11 p.m. saying the "emergency has ended."    Smith...', 'https://dailyillini.com/wp-content/uploads/2017/10/gas-leak-356x475.jpg', ' October 4, 2017 '),
(23, 'Dancing mindfulness from home to University', 'For the past two years, the Women’s Resources Center has been hosting a Dancing Mindfulness event every other week.     Dancing Mindfulness is a dance class that was organized by Holistic Health Educator Theresa Benson, who said the event is for anyone who identifies as a woman as a way to engage s...', 'https://dailyillini.com/wp-content/uploads/2017/10/MoonGala-2-475x315.jpg', ' October 4, 2017 '),
(24, 'Police blotter for Oct. 3', 'Champaign  Burglary from a motor vehicle was reported on the 2400 block of Bradley Avenue around 4 p.m. Monday. According to the report, two unknown offenders entered the victim’s car without permission and stole property.    Criminal sexual assault was reported on the 400 block of Neil Street arou...', 'https://dailyillini.com/wp-content/uploads/2016/10/A2_Blotter-475x317.jpg', ' October 4, 2017 '),
(25, 'Disability services fraternity notices decrease in membership', 'Established in 1949 at the University, Delta Sigma Omicron is the first disability services fraternity that welcomes any person with a disability. However, the fraternity has become less active in recent years.     Maureen Gilbert, coordinator of campus life at the Disability Resources and Educational...', 'https://dailyillini.com/wp-content/uploads/2017/10/YSGuvf4qPkVTHNXiorVmjgbM07_AVD1shKM51SarpglkITW1DIGcWeTwC5_Uo0yIjZpkz02EYjwPLkM5bwC9swTdsm8s2048-316x475.jpg', ' October 4, 2017 ');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
